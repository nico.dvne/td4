﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mesozoic;

namespace MesozoicConsole
{
     public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello from Mesozoic!");
            Dinosaur louis, nessie;

            louis = new Dinosaur("Louis", "Stegausaurus", 12);
            nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            List<Dinosaur> dinosaurs = new List<Dinosaur>();            
            dinosaurs.Add(louis);
            dinosaurs.Add(nessie);
            Console.WriteLine(dinosaurs.Count);//nombre de dinausaur dans la liste
            foreach (Dinosaur dino in dinosaurs) //pour tous les dino(de type dinosaur) dans la liste Dinosaurs
            {
                string phrase = String.Format("Bonjour je suis {0} le {1}", dino.getName(), dino.getSpecie());
                Console.WriteLine(phrase); 
            }
            
            
            Console.ReadKey();
        }
    }
}
