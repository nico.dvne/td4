﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicConsole;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin a Nessie.",louis.hug(nessie));
        }
        [TestMethod]
        public void Test_GetName()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Louis", louis.getName());
        }
        [TestMethod]
        public void Test_GetAge()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void Test_GetSpecie()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
        }

        [TestMethod]
        public void TestDinosaurSetName()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            louis.setName("Freddy");
            Assert.AreEqual("Freddy", louis.getName());
        }
        public void TestDinausaurSetSpecie()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            louis.setSpecie("Chien");
            Assert.AreEqual("Chien", louis.getName());
        }
        public void TestDinausautSetAge()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            louis.setAge(15);
            Assert.AreEqual(15, louis.getAge());
        }

    }
}
